#!/bin/bash

if [ "${USER}" != "root" ]; then
    echo "must be run as root"
    exit 1
fi

scriptPath="$(dirname "$(realpath "$0")")"

commonVars="${scriptPath}/tut01-common_vars"
commonSetupScript="${scriptPath}/tut01-common_setup.sh"

test -r "${commonVars}" && source "${commonVars}"
test -x "${commonSetupScript}" && "${commonSetupScript}" main

###########################################################
###### main (cs) specific fs setup ########################
###########################################################

test -d "${pathPkiBase}" || mkdir "${pathPkiBase}"
test -d "${pathCrtOut}" || mkdir "${pathCrtOut}"

tune2fs -O encrypt "$(findmnt -n -M / -o SOURCE)" &&
    echo "y" | fscrypt setup --force &&
    echo -e "protector1\npw\npw" | fscrypt encrypt --source=custom_passphrase "${pathPkiBase}" &&
    echo -e "protector2\npw\npw" | fscrypt encrypt --source=custom_passphrase "/etc/named"
fscrypt unlock "${pathPkiBase}"
fscrypt unlock "/etc/named"

###########################################################
###### main (cs) specific CA setup ########################
###########################################################

test -L "/usr/local/bin/easyrsa" || ln -s /usr/share/easy-rsa/3/easyrsa /usr/local/bin/

/usr/local/bin/easyrsa init-pki &&
    echo "${configCaName}" | /usr/local/bin/easyrsa build-ca nopass &&
    echo "yes" | /usr/local/bin/easyrsa build-server-full "${webFqdn}" nopass &&
    cp "${pathPki}/issued/${webFqdn}.crt" "${pathCrtOut}" &&
    cp "${pathPki}/private/${webFqdn}.key" "${pathCrtOut}"

cp "${pathPki}/ca.crt" "/etc/pki/ca-trust/source/anchors/" &&
    /usr/bin/update-ca-trust

###########################################################
###### main (cs) specific DNSSEC and bind setup ###########
###########################################################

configZoneFile="/var/named/named.${configDomain}"

cat <<ZONE >"${configZoneFile}"
\$ORIGIN ${configDomain}.
\$TTL 86400
@	IN	SOA	${mainFqdn}.	hostmaster.${configDomain}. (
			2001062501 ; serial
			21600      ; refresh after 6 hours
			3600       ; retry after 1 hour
			604800     ; expire after 1 week
			86400 )    ; minimum TTL of 1 day

	IN	NS	${mainFqdn}.

${mainHostname}	IN	A	${mainIp4}
${mainHostname}	IN	AAAA	${mainIp6}
${webHostname}	IN	A	${webIp4}
${webHostname}	IN	AAAA	${webIp6}
${clientHostname}	IN	A	${clientIp4}
${clientHostname}	IN	AAAA	${clientIp6}
ZONE

dnssec-keygen -a RSASHA256 -b 1024 -K /etc/named "${configDomain}"
dnssec-keygen -f KSK -a RSASHA256 -b 2048 -K /etc/named "${configDomain}"

sed -i '/^$INCLUDE.*$/d' "${configZoneFile}"
find /etc/named -name "*.key" | while read -r keyfile; do
    echo "\$INCLUDE $keyfile" >>"${configZoneFile}"
done

dnssec-signzone -S -K /etc/named -N INCREMENT -o "${configDomain}" -t "${configZoneFile}"

test -e "/etc/named.conf.orig" || cp "/etc/named.conf" "/etc/named.conf.orig"
cat "/etc/named.conf.orig" <(
    cat <<EOF

zone "${configDomain}" {
        type primary;
        file "named.${configDomain}.signed";
};
EOF
) >/etc/named.conf && systemctl restart named
