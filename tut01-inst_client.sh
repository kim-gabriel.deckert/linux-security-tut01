#!/bin/bash

if [ "${USER}" != "root" ]; then
    echo "must be run as root"
    exit 1
fi

scriptPath="$(dirname "$(realpath "$0")")"

commonVars="${scriptPath}/tut01-common_vars"
commonSetupScript="${scriptPath}/tut01-common_setup.sh"

test -r "${commonVars}" && source "${commonVars}"
test -x "${commonSetupScript}" && "${commonSetupScript}" client

###########################################################
###### client (c) specific setup ##########################
###########################################################

scp -o "StrictHostKeyChecking=no" root@"${mainHostname}":"${pathPki}/ca.crt" "/etc/pki/ca-trust/source/anchors/" &&
    /usr/bin/update-ca-trust
