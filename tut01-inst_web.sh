#!/bin/bash

if [ "${USER}" != "root" ]; then
    echo "must be run as root"
    exit 1
fi

scriptPath="$(dirname "$(realpath "$0")")"

commonVars="${scriptPath}/tut01-common_vars"
commonSetupScript="${scriptPath}/tut01-common_setup.sh"

test -r "${commonVars}" && source "${commonVars}"
test -x "${commonSetupScript}" && "${commonSetupScript}" web

###########################################################
###### web (ws) specific setup ############################
###########################################################

pathCert="/etc/ssl/${webFqdn}/${webFqdn}.crt"
pathKey="/etc/ssl/${webFqdn}/${webFqdn}.key"

scp -o "StrictHostKeyChecking=no" root@"${mainHostname}":"${pathPki}/ca.crt" "/etc/pki/ca-trust/source/anchors/" &&
    /usr/bin/update-ca-trust
scp -r -o "StrictHostKeyChecking=no" root@"${mainHostname}":"${pathCrtOut}" "/etc/ssl/"

systemctl enable httpd

sed -ri "s#^(\s*SSLCertificateFile)\s+.*#\1 ${pathCert}#g" /etc/httpd/conf.d/ssl.conf
sed -ri "s#^(\s*SSLCertificateKeyFile)\s+.*#\1 ${pathKey}#g" /etc/httpd/conf.d/ssl.conf

semanage fcontext -a -t httpd_sys_content_t "/etc/ssl(/.*)"
restorecon -r /etc/ssl

systemctl restart httpd

test -d "/var/www/html" || mkdir -p "/var/www/html"

cat <<EOF >"/var/www/html/index.html"
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to My Website</title>
</head>
<body>
    <h1>Hello, world!</h1>
    <p>This is a basic website served by Apache on Fedora.</p>
</body>
</html>
EOF
