#!/bin/bash

if [ "${USER}" != "root" ]; then
    echo "must be run as root"
    exit 1
fi

scriptPath="$(dirname "$(realpath "$0")")"
commonVars="${scriptPath}/tut01-common_vars"
test -r "${commonVars}" && source "${commonVars}"

dhclient "${confignic0}"
getent passwd | grep -q "${configUser}:" || useradd -r -s /bin/bash -g wheel "${configUser}"

if [ -n "${1}" ]; then
    hostType="${1}"
    case "${hostType}" in
    "client")
        hostnamectl set-hostname "${clientHostname}"
        grep -q "${clientIp4}/${networkIp4Prefix}" <(ip -family inet address show dev "${confignic1}") ||
            ip -family inet address add "${clientIp4}/${networkIp4Prefix}" dev "${confignic1}"
        grep -q "${clientIp6}/${networkIp6Prefix}" <(ip -family inet6 address show dev "${confignic1}") ||
            ip -family inet6 address add "${clientIp6}/${networkIp6Prefix}" dev "${confignic1}"
        cat <<EOCLIENT >/etc/hosts
127.0.0.1 ${clientFqdn} ${clientHostname}
::1 ${clientFqdn} ${clientHostname}
EOCLIENT
        dnfPackages=("openssl" "cryptsetup" "openssh-server" "dhcp-client" "firefox" "ca-certificates")
        ;;
    "main")
        hostnamectl set-hostname "${mainHostname}"
        grep -q "${mainIp4}/${networkIp4Prefix}" <(ip -family inet address show dev "${confignic1}") ||
            ip -family inet address add "${mainIp4}/${networkIp4Prefix}" dev "${confignic1}"
        grep -q "${mainIp6}/${networkIp6Prefix}" <(ip -family inet6 address show dev "${confignic1}") ||
            ip -family inet6 address add "${mainIp6}/${networkIp6Prefix}" dev "${confignic1}"
        cat <<EOMAIN >/etc/hosts
127.0.0.1 ${mainFqdn} ${mainHostname} hostmaster.${configDomain} hostmaster
::1 ${mainFqdn} ${mainHostname} hostmaster.${configDomain} hostmaster
EOMAIN
        dnfPackages=("openssl" "easy-rsa" "cryptsetup" "bind" "bind-dnssec-utils" "bind-utils" "openssh-server" "dhcp-client" "fscrypt" "e2fsprogs" "ca-certificates")
        ;;
    "web")
        hostnamectl set-hostname "${webHostname}"
        grep -q "${webIp4}/${networkIp4Prefix}" <(ip -family inet address show dev "${confignic1}") ||
            ip -family inet address add "${webIp4}/${networkIp4Prefix}" dev "${confignic1}"
        grep -q "${webIp6}/${networkIp6Prefix}" <(ip -family inet6 address show dev "${confignic1}") ||
            ip -family inet6 address add "${webIp6}/${networkIp6Prefix}" dev "${confignic1}"
        cat <<EOWEB >/etc/hosts
127.0.0.1 ${webFqdn} ${webHostname}
::1 ${webFqdn} ${webHostname}
EOWEB
        dnfPackages=("openssl" "cryptsetup" "openssh-server" "dhcp-client" "httpd" "mod_ssl" "ca-certificates")
        ;;

    esac
    cat <<EOHOSTS >>/etc/hosts
${mainIp4} ${mainFqdn} ${mainHostname}
${mainIp6} ${mainFqdn} ${mainHostname}

${webIp4} ${webFqdn} ${webHostname}
${webIp6} ${webFqdn} ${webHostname}

${clientIp4} ${clientFqdn} ${clientHostname}
${clientIp6} ${clientFqdn} ${clientHostname}
EOHOSTS

    test -d "${sshDir}" || mkdir "${sshDir}"
    cat <<SSHCONF >"${sshDir}/config"
Host ${clientHostname}
    HostName ${clientHostname}.${configDomain}
    User root
    IdentityFile ${sshKeyPath}

Host ${mainHostname}
    HostName ${mainFqdn}
    User root
    IdentityFile ${sshKeyPath}

Host ${webHostname}
    HostName ${webFqdn}
    User root
    IdentityFile ${sshKeyPath}
SSHCONF
    echo "${sshPubKey}" >"${sshKeyPath}.pub"
    echo "${sshPrivKey}" >"${sshKeyPath}"
    echo "${sshPubKey}" >"${sshDir}/authorized_keys"
    chmod -R 0600 "${sshDir}"

    systemctl stop systemd-resolved &&
        systemctl disable systemd-resolved

    rm -f /etc/resolv.conf &&
        echo "nameserver ${mainIp4}" >/etc/resolv.conf &&
        echo "nameserver ${mainIp6}" >>/etc/resolv.conf &&
        echo "nameserver 8.8.8.8" >>/etc/resolv.conf &&
        echo "search ${configDomain}" >>/etc/resolv.conf

    systemctl enable sshd
    systemctl start sshd
    systemctl stop firewalld
    systemctl disable firewalld

    timedatectl set-timezone "${configTimezone}"

    systemctl stop systemd-resolved &&
        systemctl disable systemd-resolved

    rm -f /etc/resolv.conf &&
        echo "nameserver ${mainIp4}" >/etc/resolv.conf &&
        echo "nameserver ${mainIp6}" >>/etc/resolv.conf &&
        echo "nameserver 8.8.8.8" >>/etc/resolv.conf &&
        echo "search ${configDomain}" >>/etc/resolv.conf

    echo "${dnfPackages[@]}" | xargs dnf -y install
fi
